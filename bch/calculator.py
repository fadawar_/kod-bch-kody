from enum import IntEnum, unique

from bch.tables import beta_15, beta_31

beta_tables = {15: beta_15, 31: beta_31}


@unique
class Case(IntEnum):
    correct = 0
    one_error = 1
    two_errors = 2
    more_errors = 3


class BCHCalculator:
    def __init__(self):
        self._word = []
        self.s1 = []
        self.s3 = []
        self.s3_value = -1
        self.s1_value = -1

    def set_word(self, value: str):
        if len(value) not in beta_tables:
            raise ValueError("Zadané slovo má nesprávnu dĺžku. Povolené dĺžky [15, 31]")
        for char in value:
            if char not in ('0', '1'):
                raise ValueError("Slovo môže obsahovať iba znaky 0 a 1.")
            else:
                self._word.append(int(char))

    def get_word(self):
        return self._word

    word = property(get_word, set_word)

    def decode(self):
        self.reset_values()
        word_length = len(self.word)
        table = beta_tables[word_length]
        self.s1 = self.word_to_list_of_indexes(self.word)
        self.s3 = self.create_s3(self.s1, divisor=word_length)
        self.s1_value = self.addition_in_table(table, self.s1)
        self.s3_value = self.addition_in_table(table, self.s3)

        if self.s1_value == self.s3_value == 0:
            return Case.correct, self.word, []
        elif self.s3_value == self.remainder(self.s1_value * 3, word_length):
            e = [self.s1_value]
            correct_word_indexes = self.addition_words(self.s1, e)
            correct_word = list(self.list_of_indexes_to_word(correct_word_indexes, word_length))
            return Case.one_error, correct_word, e
        else:
            return self.decode_with_two_errors(table, word_length)

    def decode_with_two_errors(self, table: list, word_length: int):
        # x^2 + part1*x + part2 = 0
        # part1 = s1
        # part2 = s3 * s1^(-1) + s1^2
        s1_to_the_power_of_minus_1 = word_length - self.s1_value
        s3s1 = self.remainder(self.s3_value + s1_to_the_power_of_minus_1, word_length)
        s1_squared = self.remainder(self.s1_value * 2, word_length)
        part1 = self.s1_value
        part2 = self.addition_in_table(table, [s3s1, s1_squared])

        for i in range(len(table)):
            for j in range(len(table[i])):
                if table[i][j] == part1 and self.remainder(i+j, word_length) == part2:
                    e = [i, j]
                    correct_word_indexes = self.addition_words(self.s1, e)
                    correct_word = list(self.list_of_indexes_to_word(correct_word_indexes, word_length))
                    return Case.two_errors, correct_word, e
        return Case.more_errors, None, None

    def reset_values(self):
        self.s1 = []
        self.s3 = []
        self.s3_value = -1
        self.s1_value = -1

    @staticmethod
    def word_to_list_of_indexes(word: list):
        # Ak prva cislica je '1', vrati sa dlzka slova (napr. 15), aby to reprezentovalo hodnotu 1
        # Hodnotu 1 totizto reprezentujeme v celom algoritme ako max. dlzku, teda 15 alebo 31
        return [(index if index != 0 else len(word)) for index, value in enumerate(word) if value == 1]

    @staticmethod
    def list_of_indexes_to_word(indexes: list, word_length):
        # Ak mame dlzku slova (napr. 15) v indexoch, vratime ju ako 1
        # (lebo 1 rezprezentujeme ako 15)
        yield 0 if word_length not in indexes else 1
        for i in range(1, word_length):
            yield 0 if i not in indexes else 1

    @staticmethod
    def create_s3(s1: list, divisor):
        s3 = []
        for number in s1:
            s3.append(BCHCalculator.remainder(number * 3, divisor))
        return s3

    @staticmethod
    def remainder(number, divisor):
        remainder = number % divisor
        if number != 0 and remainder == 0:
            remainder = divisor
        return remainder

    @staticmethod
    def addition_in_table(table: list, numbers: list):
        length = len(numbers)
        if length == 0:
            return 0
        elif length == 1:
            return numbers[0]
        else:
            result = table[numbers[0]][numbers[1]]
            for i in range(2, len(numbers)):
                number = numbers[i]
                result = table[result][number]
            return result

    @staticmethod
    def addition_words(word1, word2):
        new_word = list(word1)
        for char in word2:
            if char in new_word:
                new_word.remove(char)
            else:
                new_word.append(char)
        return sorted(new_word)
