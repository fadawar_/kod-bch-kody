import unittest

from bch.calculator import BCHCalculator, Case, beta_tables


class CalculatorParseWordTestCase(unittest.TestCase):
    def setUp(self):
        self.cal = BCHCalculator()

    def test_set_correct_word(self):
        word = '000010001011100'

        try:
            self.cal.word = word
        except Exception:
            self.fail("Calculator should accept this word.")

    def test_calculator_check_word_length(self):
        w1 = '01101'  # too short
        w2 = '01011010101010101010101'  # too long

        with self.assertRaises(ValueError) as cm:
            self.cal.word = w1
        self.assertEqual(str(cm.exception), "Zadané slovo má nesprávnu dĺžku. Povolené dĺžky [15, 31]")

        with self.assertRaises(ValueError):
            self.cal.word = w2
        self.assertEqual(str(cm.exception), "Zadané slovo má nesprávnu dĺžku. Povolené dĺžky [15, 31]")

    def test_calculator_only_accept_zeros_and_ones(self):
        w = '01010abc0011111'

        with self.assertRaises(ValueError) as cm:
            self.cal.word = w
        self.assertEqual(str(cm.exception), "Slovo môže obsahovať iba znaky 0 a 1.")

    def test_has_word_attribute(self):
        word = '000010001011100'
        self.cal.word = word

        try:
            self.cal.word
        except Exception:
            self.fail("Calculator should have word attribute.")

    def test_calculator_parse_word(self):
        word = '000010001011100'
        word_list = [0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0]
        self.cal.word = word

        self.assertEqual(word_list, self.cal.word)


class CalculatorDecodeTestCase(unittest.TestCase):
    def setUp(self):
        self.cal = BCHCalculator()

    def test_decode_correct_word(self):
        self.cal.word = '000010001011100'
        word_list = [0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0]

        case, correct_word, error_word = self.cal.decode()

        self.assertEqual(Case.correct, case)
        self.assertEqual(correct_word, word_list)
        self.assertEqual(error_word, [])

    def test_decode_word_with_one_error(self):
        self.cal.word = '011001110010001'
        correct_word_list = [0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0]

        case, correct_word, error_word = self.cal.decode()

        self.assertEqual(Case.one_error, case)
        self.assertEqual(correct_word, correct_word_list)
        self.assertEqual(error_word, [14])

    def test_decode_word_with_two_errors(self):
        self.cal.word = '110111101011000'
        correct_word_list = [1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0]

        case, correct_word, error_word = self.cal.decode()

        self.assertEqual(Case.two_errors, case)
        self.assertEqual(correct_word, correct_word_list)
        self.assertEqual(error_word, [7, 8])

    def test_decode_word_with_two_errors2(self):
        # testuje hladanie v tabulke aj so zvyskami (teda ci hladame B12 = B27)
        self.cal.word = '110111110011011'
        correct_word_list = [1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0]

        case, correct_word, error_word = self.cal.decode()

        self.assertEqual(Case.two_errors, case)
        self.assertEqual(correct_word, correct_word_list)
        self.assertEqual(error_word, [13, 14])

    def test_decode_word_with_two_errors_beta_31(self):
        self.cal.word = '0010010100101010110101000000000'
        correct_word_list = [0,0,1,0,0,1,0,1,1,0,1,0,1,0,1,0,1,1,0,1,1,1,0,0,0,0,0,0,0,0,0]

        case, correct_word, error_word = self.cal.decode()

        self.assertEqual(Case.two_errors, case)
        self.assertEqual(correct_word, correct_word_list)
        self.assertEqual(error_word, [8, 20])

    def test_convert_from_word_to_indexes(self):
        word_list = [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0]

        self.assertEqual([15, 4, 8, 10, 11, 12], self.cal.word_to_list_of_indexes(word_list))

    def test_convert_from_indexes_to_word(self):
        word_list = [0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0]
        indexes = [4, 8, 10, 11, 12]

        self.assertEqual(word_list, list(self.cal.list_of_indexes_to_word(indexes, len(word_list))))

    def test_convert_from_indexes_to_word_first_one(self):
        word_list = [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0]
        indexes = [4, 8, 10, 11, 12, 15]

        self.assertEqual(word_list, list(self.cal.list_of_indexes_to_word(indexes, len(word_list))))

    def test_creation_of_s3(self):
        s1 = [0, 1, 2, 3, 4]
        s3 = self.cal.create_s3(s1, divisor=15)

        self.assertEqual([0, 3, 6, 9, 12], s3)

    def test_creation_of_s3_big_numbers(self):
        s1 = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        s3 = self.cal.create_s3(s1, divisor=15)

        self.assertEqual([15, 3, 6, 9, 12, 15, 3, 6, 9, 12, 15], s3)

    def test_addition_in_beta_tables(self):
        list1 = [4, 8, 10, 11, 12]
        list2 = [12, 9, 15, 3, 6]
        list3 = [1, 2, 5, 6, 7, 10, 14]
        list4 = [3, 6, 15, 3, 6, 15, 12]
        list5 = [15, 3, 9, 12, 15, 3, 9, 15, 3]
        table = beta_tables[15]

        self.assertEqual(0, self.cal.addition_in_table(table, list1))
        self.assertEqual(0, self.cal.addition_in_table(table, list2))
        self.assertEqual(14, self.cal.addition_in_table(table, list3))
        self.assertEqual(12, self.cal.addition_in_table(table, list4))
        self.assertEqual(5, self.cal.addition_in_table(table, list5))

    def test_addition_in_beta_tables_edge_cases(self):
        list1 = []
        list2 = [12]
        table = beta_tables[15]

        self.assertEqual(0, self.cal.addition_in_table(table, list1))
        self.assertEqual(12, self.cal.addition_in_table(table, list2))

    def test_addition_words(self):
        word1 = [0, 1]
        word2 = [2, 3]
        word3 = [1, 2]

        self.assertEqual([0, 1, 2, 3], self.cal.addition_words(word1, word2))
        self.assertEqual([0, 1, 2, 3], self.cal.addition_words(word2, word1))
        self.assertEqual([0, 2], self.cal.addition_words(word1, word3))
        self.assertEqual([1, 3], self.cal.addition_words(word3, word2))

    def test_remainder(self):
        self.assertEqual(0, self.cal.remainder(0, 15))
        self.assertEqual(1, self.cal.remainder(1, 15))
        self.assertEqual(14, self.cal.remainder(14, 15))
        self.assertEqual(15, self.cal.remainder(15, 15))
        self.assertEqual(15, self.cal.remainder(30, 15))
        self.assertEqual(15, self.cal.remainder(15, 15))
        self.assertEqual(1, self.cal.remainder(16, 15))
        self.assertEqual(6, self.cal.remainder(21, 15))
