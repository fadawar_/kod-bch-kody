from django.shortcuts import render

from bch.calculator import BCHCalculator


def home_page(request):
    if request.method == 'POST':
        try:
            text = request.POST['received_word']
            cal = BCHCalculator()
            cal.word = text
            case, correct_word, e = cal.decode()
            return render(request, 'home.html',
                          {'results': True, 'received_word': text, 'case': int(case), 'correct_word': correct_word,
                           'error_word': e, 's1': cal.s1, 's1_value': cal.s1_value, 's3': cal.s3,
                           's3_value': cal.s3_value})
        except ValueError as e:
            return render(request, 'home.html', {'errors': str(e)})
    return render(request, 'home.html')


def about_page(request):
    return render(request, 'about.html')
